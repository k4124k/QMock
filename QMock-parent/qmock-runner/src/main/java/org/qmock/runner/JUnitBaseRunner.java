package org.qmock.runner;


import org.apache.log4j.PropertyConfigurator;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.RunnerScheduler;
import org.junit.runners.model.Statement;
import org.qmock.runner.anno.InterceptorClass;
import org.qmock.runner.anno.ThreadRunner;
import org.qmock.runner.statement.Interceptor;
import org.qmock.runner.statement.InterceptorStatement;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 王天庆
 * */
public class JUnitBaseRunner extends Feeder{
    //private Logger logger = Logger.getLogger(JUnitBaseRunner.class);
    public JUnitBaseRunner(final Class<?> klass)
            throws InitializationError {
        super(klass);
        PropertyConfigurator.configure("resource/log4j.properties");
        setScheduler(new RunnerScheduler() {
            ExecutorService executorService = Executors.newFixedThreadPool(
                    klass.isAnnotationPresent(ThreadRunner.class) ?
                            klass.getAnnotation(ThreadRunner.class).threads() :
                            (int) (Runtime.getRuntime().availableProcessors() * 1.5),
                    new NamedThreadFactory(klass.getSimpleName()));
            CompletionService<Void> completionService = new ExecutorCompletionService<Void>(executorService);
            Queue<Future<Void>> tasks = new LinkedList<Future<Void>>();

            public void schedule(Runnable childStatement) {
                tasks.offer(completionService.submit(childStatement, null));
            }


            public void finished() {
                try {
                    while (!tasks.isEmpty())
                        tasks.remove(completionService.take());
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } finally {
                    while (!tasks.isEmpty())
                        tasks.poll().cancel(true);
                    executorService.shutdownNow();
                }
            }

        });
    }
    static final class NamedThreadFactory implements ThreadFactory {
        static final AtomicInteger poolNumber = new AtomicInteger(1);
        final AtomicInteger threadNumber = new AtomicInteger(1);
        final ThreadGroup group;

        NamedThreadFactory(String poolName) {
            group = new ThreadGroup(poolName + "-" + poolNumber.getAndIncrement());
        }


        public Thread newThread(Runnable r) {
            return new Thread(group, r, group.getName() + "-thread-" + threadNumber.getAndIncrement(), 0);
        }
    }


    protected Statement methodInvoker(FrameworkMethod method, Object test) {
        InterceptorStatement statement = new InterceptorStatement(method, test);
        if(test.getClass().isAnnotationPresent(InterceptorClass.class)){
            InterceptorClass anno = test.getClass().getAnnotation(InterceptorClass.class);
            Class<?>[] clazzs = anno.value();
            try{
                for(Class<?> clazz : clazzs){
                    statement.addInterceptor((Interceptor)clazz.newInstance());
                }
            }catch(IllegalAccessException ilex){
                ilex.printStackTrace();
            }catch(InstantiationException e){
                e.printStackTrace();
            }
        }
        return statement;
    }



}
