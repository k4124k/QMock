package org.qmock.runner;

import org.qmock.runner.exception.ConfigWrongException;

import java.io.File;
import java.util.Map;


public class ConfigParser {
	private static Map<String,String> parse(){
		if(!new File("resource"+File.separator+"config.properties").exists()){
			throw new ConfigWrongException("config配置信息无法扫描到，也许是无法找到config.properties文件！请检查配置文件");
		}
		return PropertiesTools.getPropertiesMap("resource" + File.separator + "config.properties");
	}
	/**返回我们设置的Base-name的值*/
	public static String getBaseName(){
		return parse().get("base-name");
	}

}



