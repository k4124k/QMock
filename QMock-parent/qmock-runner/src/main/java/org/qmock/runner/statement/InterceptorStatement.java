package org.qmock.runner.statement;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.qmock.runner.anno.Retry;
import org.qmock.runner.exception.TestFailedError;
import org.qmock.runner.runtime.RuntimeMethod;

public class InterceptorStatement extends Statement{
	private Logger logger = Logger.getLogger(InterceptorStatement.class);
	private final FrameworkMethod testMethod;
    private Object target;
    private int times=0;
	public InterceptorStatement(FrameworkMethod testMethod, Object target) {
		this.testMethod=testMethod;
		this.target=target;
	}
	private List<Interceptor> interceptors = new ArrayList<Interceptor>();

	@Override
	public void evaluate() throws Throwable {
        for(Interceptor interceptor:interceptors){
            interceptor.interceptorBefore(this.testMethod,this.target);
        }
        String className=testMethod.getMethod().getDeclaringClass().getName();
        String name="Case:"+className.substring(className.lastIndexOf(".")+1, className.length())+"=>"+testMethod.getName();
        RuntimeMethod.setName(name);

        if(this.testMethod.getMethod().isAnnotationPresent(Retry.class)){
            times=this.testMethod.getMethod().getAnnotation(Retry.class).value();
            logger.info("["+ RuntimeMethod.getName()+"]>>>>>>"+this.testMethod.getName()+">>>这个case执行失败的话会被重新执行"+times+"次");
            //System.out.println(this.testMethod.getMethod().getDeclaringClass().getName());
        }else if(this.testMethod.getMethod().getDeclaringClass().isAnnotationPresent(Retry.class)){
            times=this.testMethod.getMethod().getDeclaringClass().getAnnotation(Retry.class).value();
            logger.info("["+ RuntimeMethod.getName()+"]>>>>>>["+this.testMethod.getName()+"]>>>这个case执行失败的话会被重新执行"+times+"次");
        }else{
            this.times=0;
        }
		logger.info("["+ RuntimeMethod.getName()+"]*******************测试用例["+this.testMethod.getName()+"]开始执行*****************");
		for(int i=0;i<=times;i++){
                try{
                    testMethod.invokeExplosively(target);
                    break;
                }catch(Exception e){
                    logger.error("["+ RuntimeMethod.getName()+"]用例执行失败了,异常信息->"+e.getMessage());
                    if(i==times){
                        throw new TestFailedError(this.testMethod.getName()+"]用例执行失败了！",e);
                    }else{
                        logger.info("["+ RuntimeMethod.getName()+"]用例执行失败，重新执行失败的方法-->"+testMethod.getName());
                    }
                }
            
        }

		for(Interceptor interceptor:interceptors){
			interceptor.interceptorAfter(this.testMethod,this.target);
		}
        logger.info("["+ RuntimeMethod.getName()+"]*******************测试用例["+testMethod.getName()+"]执行结束****************");
        RuntimeMethod.setName(null);
	}
	
	
	public void addInterceptor(Interceptor interceptor){
        interceptors.add(interceptor);
	}

    public void removeInterceptor(Interceptor interceptor){
        interceptors.remove(interceptor);
    }

    public FrameworkMethod getTestMethod() {
        return testMethod;
    }

    public Object getTarget() {
        return target;
    }
}
