package com.qmock.server.test;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: tianqing.wang
 * Date: 13-8-13
 * Time: 下午5:34
 * To change this template use File | Settings | File Templates.
 */
public class UmpayReadyHttpSender {

    public static void sendRequest(String url){
        System.out.println("开始发送请求....................");
        HttpClient httpClient =new DefaultHttpClient();
        HttpPost httpPost=new HttpPost(url);
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("service","credit_direct_pay"));
        pairs.add(new BasicNameValuePair("charset","UTF-8"));
        pairs.add(new BasicNameValuePair("mer_id","12345678"));
        pairs.add(new BasicNameValuePair("sign_type","RSA"));
        pairs.add(new BasicNameValuePair("sign","ooo"));
        pairs.add(new BasicNameValuePair("version","4.0"));
        pairs.add(new BasicNameValuePair("order_id","1234567890"));
        pairs.add(new BasicNameValuePair("mer_date","20130813"));
        pairs.add(new BasicNameValuePair("amount","1111.11"));
        pairs.add(new BasicNameValuePair("amt_type","RMB"));
        pairs.add(new BasicNameValuePair("pay_type","CREDITCARD"));
        pairs.add(new BasicNameValuePair("card_id","102010201021000301"));
        pairs.add(new BasicNameValuePair("valid_date","1209"));
        pairs.add(new BasicNameValuePair("cvv2","123456"));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse response=httpClient.execute(httpPost);
            System.out.println(EntityUtils.toString(response.getEntity(), HTTP.UTF_8));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        httpPost.abort();
        httpClient.getConnectionManager().shutdown();
    }
}
