package com.qmock.server.test;

import com.qmock.pay.anno.Mock;
import com.qmock.pay.anno.ThenReturn;
import com.qmock.pay.runner.Bank;
import com.qmock.pay.runner.PayMockRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author tianqing.wang
 */
@RunWith(PayMockRunner.class)
@Mock(Bank.UMPAY)
public class ServerTest {

    @Test
    @ThenReturn("response/UmpayResponse.xml")
    public void test(){
        UmpayReadyHttpSender.sendRequest("http://192.168.234.186:9999/umpay");
    }

}
