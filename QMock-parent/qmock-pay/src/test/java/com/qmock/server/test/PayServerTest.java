package com.qmock.server.test;
import com.qmock.pay.http.InfoMap;
import org.junit.Test;
import com.qmock.pay.common.Log;
import com.qmock.pay.http.PayMockServer;
import com.qmock.pay.runner.Bank;


public class PayServerTest {

	@Test
	public void serverTest() throws Exception, InterruptedException{
		Log.init();
		PayMockServer server = new PayMockServer();
		server.mockBank(Bank.UMPAY).thenReturn(InfoMap.getExpectResponse("info")).url("/umpay").start();
        while(true){
            Thread.sleep(1000);
        }
	}
}
