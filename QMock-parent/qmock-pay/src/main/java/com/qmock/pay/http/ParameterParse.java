package com.qmock.pay.http;

import org.apache.log4j.Logger;
import org.webbitserver.HttpRequest;

import java.util.HashMap;
import java.util.Set;
import java.util.Map;

/**
 * @author tianqing.wang
 */
public class ParameterParse {
    private Map<String,String> parameters;
    private Set<String> parameterKeys;
    private Logger logger = Logger.getLogger(this.getClass());
    public ParameterParse(HttpRequest httpRequest){
        this.parameters=new HashMap<String, String>();
        if(httpRequest.method().toUpperCase().equals("POST")){
            logger.info("请求方法为POST");
            this.parameterKeys=httpRequest.postParamKeys();
            for(String key:this.parameterKeys){
                this.parameters.put(key,httpRequest.postParam(key));
                logger.info("请求的参数--> key: "+key+", value: "+httpRequest.postParam(key));
            }
        }else if(httpRequest.method().toUpperCase().equals("GET")){
            logger.info("请求的方法为GET");
            this.parameterKeys=httpRequest.queryParamKeys();
            for(String key:this.parameterKeys){
                this.parameters.put(key,httpRequest.queryParam(key));
                logger.info("请求的参数--> key: "+key+", value: "+httpRequest.postParam(key));
            }
        }
    }

    public Map<String,String> getParameters(){
        return this.parameters;
    }

    public Set<String> getParameterKeys(){
        return this.parameterKeys;
    }

    public String remove(String key){
        return this.getParameters().remove(key);
    }
}
