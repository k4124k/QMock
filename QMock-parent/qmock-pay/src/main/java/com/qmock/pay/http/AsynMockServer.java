package com.qmock.pay.http;



/**
 * @author tianqing.wang
 */
public interface AsynMockServer {

    public PayHttpResponse callBack(String baseUrl);

    public PayHttpResponse callBack();
}
