package com.qmock.pay.umpay.service;

import com.qmock.pay.common.DateFormat;
import com.qmock.pay.exception.SignEncException;
import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.runner.Service;
import com.qmock.pay.security.UmpayRSA;
import com.qmock.pay.umpay.UmpayParameters;
import com.qmock.pay.umpay.UmpayStringUtils;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import com.qmock.pay.umpay.handler.UmpayHttpResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * 这个类对应了预授权查询业务。
 */
public class UmpayPayAuthDirectQueryService implements Service {
    private UmpayHttpRequest resquest;
    private Map<String,String> defaultParams;
    public UmpayPayAuthDirectQueryService(UmpayHttpRequest resquest){
        this.resquest=resquest;
        this.defaultParams=new HashMap<String, String>();
        this.defaultParams.put(UmpayParameters.SIGN_TYPE,"RSA");
        this.defaultParams.put(UmpayParameters.TRADE_NO, DateFormat.format("YYYYMMDDHHMMSS"));
        this.defaultParams.put(UmpayParameters.MER_DATE,resquest.getParameterValue("mer_date"));
        this.defaultParams.put(UmpayParameters.PAY_DATE,DateFormat.format("YYYYMMDD"));
        this.defaultParams.put(UmpayParameters.REQ_AMT,resquest.getParameterValue("amount"));
        this.defaultParams.put(UmpayParameters.PAY_AMT,"付款金额");
        this.defaultParams.put(UmpayParameters.AMT_TYPE,"RMB");
        this.defaultParams.put(UmpayParameters.PAY_TYPE,"详见支付方式说明");
        this.defaultParams.put(UmpayParameters.TRADE_STATE,"详见订单状态说明");
        this.defaultParams.put(UmpayParameters.RET_CODE,"详见附录");
    }
    @Override
    public String getHttpResponseContent(ExpectResponse expectResponse) throws SignEncException {
        Map<String,String> responseparams = expectResponse.getResponse();
        this.defaultParams.putAll(responseparams);
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign", UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public String getHttpResponseContent() throws SignEncException {
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign",UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public Map<String, String> getResponseParameters() {
        return this.defaultParams;
    }
}
