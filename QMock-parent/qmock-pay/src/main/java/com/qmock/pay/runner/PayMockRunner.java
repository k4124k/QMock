package com.qmock.pay.runner;

import com.qmock.pay.common.Log;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.qmock.runner.JUnitBaseRunner;
import org.qmock.runner.anno.InterceptorClass;
import org.qmock.runner.statement.Interceptor;
import org.qmock.runner.statement.InterceptorStatement;


/**
 * 专门准对MOCK银行的运行器，支持期待值和返回值设定。
 * @author tianqing.wang
 */
public class PayMockRunner extends JUnitBaseRunner {
    //private PayMockServer payMockServer;
    //private Logger logger = Logger.getLogger(PayMockRunner.class);
    public PayMockRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    public Statement methodInvoker(FrameworkMethod method,Object test){
        InterceptorStatement statement = new InterceptorStatement(method, test);
        statement.addInterceptor(new PayInterceptor());
        if(test.getClass().isAnnotationPresent(InterceptorClass.class)){
            InterceptorClass anno = test.getClass().getAnnotation(InterceptorClass.class);
            Class<?>[] clazzs = anno.value();
            try{
                for(Class<?> clazz : clazzs){
                    statement.addInterceptor((Interceptor)clazz.newInstance());
                }
            }catch(IllegalAccessException ilex){
                ilex.printStackTrace();
            }catch(InstantiationException e){
                e.printStackTrace();
            }
        }
        return statement;
    }
}
