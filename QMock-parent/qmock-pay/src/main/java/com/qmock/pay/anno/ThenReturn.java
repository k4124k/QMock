package com.qmock.pay.anno;

import java.lang.annotation.*;

/**
 * @author tianqing.wang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface ThenReturn {
    //默认请求的xml文件路径
    String value();
}
