package com.qmock.pay.exception;

/**
 * Created with IntelliJ IDEA.
 * User: tianqing.wang
 * Date: 13-8-8
 * Time: 下午7:42
 * To change this template use File | Settings | File Templates.
 */
public class CertInitializeException extends Exception {

    public CertInitializeException(){
        super();
    }

    public CertInitializeException(String message){
        super(message);
    }

    public CertInitializeException(String message,Exception e){
        super(message);
    }
}
