package com.qmock.pay.runner;


import com.qmock.pay.exception.SignEncException;
import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.http.MockRequest;

import java.util.Map;


/**
 * @author tianqing.wang
 */
public interface Service extends MockRequest{

    public String getHttpResponseContent(ExpectResponse expectResponse) throws SignEncException;

    public String getHttpResponseContent() throws SignEncException;

    public Map<String,String> getResponseParameters();
}
