package com.qmock.pay.common;

import org.apache.log4j.PropertyConfigurator;

public class Log {
	public static void init(){
		PropertyConfigurator.configure("resource/log4j.properties");
	}

}
