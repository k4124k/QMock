package com.qmock.pay.anno;

import java.lang.annotation.*;

/**
 * @auther tianqing.wang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Inherited
public @interface When {
    /**request内存的是xml路径*/
    String request();
}
