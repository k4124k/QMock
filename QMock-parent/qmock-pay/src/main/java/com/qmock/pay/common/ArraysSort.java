package com.qmock.pay.common;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: tianqing.wang
 * Date: 13-8-6
 * Time: 下午1:39
 * To change this template use File | Settings | File Templates.
 */
public class ArraysSort {

    /**忽略大小写进行数组排序*/
    public static String[] sortCaseInsensitive(String[] strs){
        String[] newStrings = strs.clone();
        Arrays.sort(newStrings,String.CASE_INSENSITIVE_ORDER);
        return newStrings;
    }

    /**不忽略大小写进行数组倒序排序*/
    public static String[] reverseSort(String[] strs){
        String[] newStrings = strs.clone();
        Arrays.sort(newStrings, Collections.reverseOrder());
        return newStrings;
    }

    /**忽略大小写进行数组倒序排序*/
    public static String[] reverseSortCaseInsensitive (String[] strs){
        String[] newStrings = strs.clone();
        Arrays.sort(newStrings,String.CASE_INSENSITIVE_ORDER);
        Collections.reverse(Arrays.asList(newStrings));
        return newStrings;
    }

    /**不忽略大小写进行数组排序*/
    public static String[] sort(String[] strs){
        String[] newStrings = strs.clone();
        Arrays.sort(newStrings);
        return newStrings;
    }
}