package com.qmock.pay.http;

import com.qmock.pay.exception.SignEncException;

/**
 * mock是根据请求和响应两个方面设计的，这个mockrequest是请求后的总处理接口。
 */
public interface MockRequest {

    public <T>T getHttpResponseContent() throws SignEncException;
}
