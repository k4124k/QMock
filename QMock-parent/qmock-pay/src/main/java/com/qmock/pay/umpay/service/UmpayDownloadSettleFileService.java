package com.qmock.pay.umpay.service;

import com.qmock.pay.exception.SignEncException;
import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.runner.Service;
import com.qmock.pay.security.UmpayRSA;
import com.qmock.pay.umpay.UmpayStringUtils;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import com.qmock.pay.umpay.handler.UmpayHttpResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tianqing.wang
 */
public class UmpayDownloadSettleFileService implements Service {
    private UmpayHttpRequest resquest;
    private Map<String,String> defaultParams;
    public UmpayDownloadSettleFileService(UmpayHttpRequest resquest){
        this.resquest=resquest;
        this.defaultParams=new HashMap<String, String>();

    }

    @Override
    public String getHttpResponseContent(ExpectResponse expectResponse) throws SignEncException {
        Map<String,String> responseparams = expectResponse.getResponse();
        this.defaultParams.putAll(responseparams);
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        try {
            this.defaultParams.put("sign", UmpayRSA.getInstance().sign(signString));
        } catch (Exception e) {
            throw new RuntimeException("签名的时候出现了错误",e);
        }
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public String getHttpResponseContent() throws SignEncException {
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        try {
            this.defaultParams.put("sign",UmpayRSA.getInstance().sign(signString));
        } catch (Exception e) {
            throw new RuntimeException("签名的是偶出现了错误");
        }
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public Map<String, String> getResponseParameters() {
        return this.defaultParams;
    }

}
