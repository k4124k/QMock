package com.qmock.pay.http;

/**
 * 这个类是期望结果的接口，通过这个接口来获取定义好的期望结果。
 * @author tianqing.wang
 */
public interface ExpectResponseInterface {

    public <T> T getResponse();
}
