package com.qmock.pay.umpay.service;

import com.qmock.pay.common.DateFormat;
import com.qmock.pay.common.UmpayConstValue;
import com.qmock.pay.exception.SignEncException;
import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.runner.Service;
import com.qmock.pay.security.UmpayRSA;
import com.qmock.pay.umpay.UmpayParameters;
import com.qmock.pay.umpay.UmpayStringUtils;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import com.qmock.pay.umpay.handler.UmpayHttpResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tianqing.wang
 */
public class UmpayPreAuthDirectReqService implements Service{
    private UmpayHttpRequest resquest;
    private Map<String,String> defaultParams;
    public UmpayPreAuthDirectReqService(UmpayHttpRequest resquest){
        this.resquest=resquest;
        this.defaultParams=new HashMap<String, String>();
        this.defaultParams.put(UmpayParameters.SIGN_TYPE,"RSA");
        this.defaultParams.put(UmpayParameters.MER_ID,resquest.getParameterValue("mer_id"));
        this.defaultParams.put(UmpayParameters.VERSION, UmpayConstValue.UMPAY_VERSION);
        this.defaultParams.put(UmpayParameters.ORDER_ID,resquest.getParameterValue("order_id"));
        this.defaultParams.put(UmpayParameters.TRADE_NO, DateFormat.format("yyyyMMddhhmmss"));
        this.defaultParams.put(UmpayParameters.MER_DATE,resquest.getParameterValue("mer_date"));
        this.defaultParams.put(UmpayParameters.MEDIA_ID,resquest.getParameterValue("media_id"));
        this.defaultParams.put(UmpayParameters.MEDIA_TYPE,resquest.getParameterValue("media_type"));
        this.defaultParams.put(UmpayParameters.AMOUNT,resquest.getParameterValue("amount"));
        this.defaultParams.put(UmpayParameters.AMT_TYPE,"RMB");
        this.defaultParams.put(UmpayParameters.PAY_TYPE,"PERAUTHERQ");
        this.defaultParams.put(UmpayParameters.TRADE_STATE,"TRADE_SUCCESS");
        this.defaultParams.put(UmpayParameters.RET_CODE,"0000");
    }

    @Override
    public String getHttpResponseContent(ExpectResponse expectResponse) throws SignEncException {
        Map<String,String> responseparams = expectResponse.getResponse();
        this.defaultParams.putAll(responseparams);
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign", UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public String getHttpResponseContent() throws SignEncException {
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign",UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public Map<String, String> getResponseParameters() {
        return this.defaultParams;
    }
}
