package com.qmock.pay.umpay;

import com.qmock.pay.runner.Service;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import com.qmock.pay.umpay.service.UmpayService;

/**
 *@author tianqing.wang
 */
public class Umpay {
    //--------------------------Umpay的订单业务--------------------
    //--------------------------标准业务-----------------------
    /**下订单业务   service接口->pay_req*/
    public static final String PAY_REQ="pay_req";
    /**商户后台下单数据响应----需要异步实现（同时提供同步的方式来实现订单数据和后台数据的关联关系。）平台到商户*异步
     * **/
    public static final String ORDER_SERVER_ANALYSIS="order_server_analysis";
    /**商户撤销交易   service接口->mer_cancel */
    public static final String MER_CANCEL="mer_cancel";
    /**交易结果通知----需要异步通知客户（通过客户端的数据进行模拟返回响应。平台到商户*异步
     * service接口->pay_result_notify  公用的接口service
     * **/
    public static final String PAY_RESULT_NOTIFY="pay_result_notify";
    /***订单查询业务   service接口->query_orderm*/
    public static final String QUERY_ORDERM="query_orderm";
    /**商户退款     service接口->mer_refund*/
    public static final String MER_REFUND="mer_refund";
    /**清算数据对账*  商户->平台   service接口->download_settle_file*/
    public static final String DOWNLOAD_SETTLE_FILE = "download_settle_file";

    //-------------------------预授权业务--------------------------
    /**预授权申请  service接口->pre_auth_direct_req  商户->平台->商户*/
    public static final String PRE_AUTH_DIRECT_REQ = "pre_auth_direct_req";
    /**预授权完成 sercice接口->pre_auth_direct_pay 商户->平台->商户*/
    public static final String PRE_AUTH_DIRECT_PAY = "pre_auth_direct_req";
    /***预授权撤销 service接口->pre_auth_direct_cancel   商户->平台->商户*/
    public static final String PRE_AUTH_DIRECT_CANCEL = "pre_auth_direct_req";
    /**预授权结果通知   异步实现   service接口->pre_auth_notify  平台->商户*/
    public static final String PRE_AUTH_NOTIFY = "pre_auth_notify";
    /**预授权查询 商户->平台->商户  service接口->pre_auth_direct_query */
    public static final String PRE_AUTH_DIRECT_QUERY = "pre_auth_direct_query";
    /**商户退费 商户->平台->商户 service接口->pre_auth_direct_refund*/
    public static final String PRE_AUTH_DIRECT_refund = "pre_auth_direct_query";
    /**清算数据对账  商户->平台  需要异步实现*/
    public static final String PRE_AUTH_DIRECT_SETTLE = "pre_auth_direct_settle";

    //--------------------------信用卡直连支付业务-------------------
    /***商户向平台下订单  商户->平台 service接口-> credit_direct_pay*/
    public static final String CREDIT_DIRECT_PAY = "credit_direct_pay";
    /**商户后台下单响应数据   平台->商户  内部实现解析。 支持异步查询和响应 */
    public static final String CREDIT_ORDER_ANALYSIS = "credit_order_analysit";
    //信用卡支付的方式和标准流程是一样的，只有支付的义务接口不一样，查询，退费等服务接口参考标准业务。

    //--------------------------借记卡直连支付-----------------------
    /**商户向平台下订单 service接口->debit_direct_pay**/
    public static final String DEBIT_DIRECT_PAY="debit_direct_pay";

    //--------------------------退费查询接口服务---------------------
    /**商户支付订单退费查询 service接口->mer_order_refund_query*/
    public static final String MER_ORDER_REFUND_QUERY="mer_order_refund_query";
    /**商户退费订单的查询 service接口->mer_refundno_query*/
    public static final String MER_REFUNDNO_QUERY="mer_refundno_query";

    //--------------------------WAP接口的业务实现-------------------
    /**分账通知 service接口-> split_req_result*/
    public static final String SPLIT_REQ_RESULT="split_req_result";
    /**WAP后端支付请求  service接口-> pay_req_split_back*/
    public static final String PAY_REQ_SPLIT_BACK="pay_req_split_back";
    /**分账请求 service接口-> split_req*/
    public static final String SPLIT_REQ="split_req";
    /**分账退费请求 servcie接口-> split_refund_req*/
    public static final String SPLIT_REFUND_REQ="split_refund_req";
    /**分账退费结果通知 service接口-> split_refund_result*/
    public static final String SPLIT_REFUND_RESULT="split_refund_result";
    /***/

    //--------------------------Umpay的业务实现----------------------

    public static Service getService(UmpayHttpRequest umpayHttpResquest){
        String service = umpayHttpResquest.getService();
        if(service!=null){
            UmpayService umpayService=Enum.valueOf(UmpayService.class,service.toUpperCase());
            return umpayService.service(umpayHttpResquest);
        }else{
            throw new RuntimeException("HTTP请求信息不存在正确的service标识信息，请求重新发送信息。");
        }
    }

    public static boolean isUmpayService(String serviceName){
        return true;
    }
}
