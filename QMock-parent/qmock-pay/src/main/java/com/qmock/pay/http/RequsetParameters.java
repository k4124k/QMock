package com.qmock.pay.http;

/**
 * @author tianqing.wang
 */
public interface RequsetParameters {
    /****如果我们的请求是通过post或者get的方法传递的，就实现topair的方法。*/
    public ParameterPairs toPairs();
    /***如果请求的参数是序列化对象的话，我们就实现序列化的方法 */
    public Object toSerialize();
}
