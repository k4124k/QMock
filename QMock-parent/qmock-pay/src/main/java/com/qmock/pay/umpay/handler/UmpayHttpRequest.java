package com.qmock.pay.umpay.handler;

import com.qmock.pay.http.ParameterParse;
import com.qmock.pay.umpay.UmpayParameters;
import org.webbitserver.HttpRequest;

import java.util.Map;

/**
 * 这个请求类是基于整个umpay的请求参数的获取，所有的请求参数都在这个类里面获取。
 * @author
 */
public class UmpayHttpRequest {
    public Map<String,String> requestParameterPair;
    private ParameterParse parameterParse;
    public UmpayHttpRequest(HttpRequest httpRequest){
        parameterParse=new ParameterParse(httpRequest);
        requestParameterPair=parameterParse.getParameters();
    }
    /**得到这个服务的接口参数。通过参数来确定这个业务流以及确定响应。*/
    public String getService(){
        return this.requestParameterPair.get(UmpayParameters.SERVICE);
    }
    public Map<String,String> getRequestParameters(){
        return this.requestParameterPair;
    }

    public String getParameterValue(String key){
        return this.requestParameterPair.get(key);
    }
}
