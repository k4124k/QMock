package com.qmock.pay.runner;

import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.http.ExpectResponseInterface;
import com.qmock.pay.http.MockHttpHandle;
import com.qmock.pay.umpay.handler.UmpayHttpHandler;
import com.qmock.pay.umpay.service.UmpayService;
import org.webbitserver.HttpHandler;

import java.io.File;

/**
 * @author tianqing.wang
 */
public enum Bank {
    MOCK(){
        @Override
        public <T> T bankService(String service) {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public HttpHandler handler(ExpectResponseInterface expectResponse, Service service, String url) {
            return null;
        }

        @Override
        public HttpHandler handler(ExpectResponseInterface expectResponse, String url) {
            return null;
        }

        public HttpHandler handler(File dir){
            return new MockHttpHandle(dir);
        }
    },
    UMPAY(){
        public UmpayService bankService(String service) {
            return Enum.valueOf(UmpayService.class,service.toUpperCase());
        }
        public HttpHandler handler(ExpectResponseInterface expectResponse,Service service,String url){
            return new UmpayHttpHandler(expectResponse,service,url);
        }

        @Override
        public HttpHandler handler(ExpectResponseInterface expectResponse,String url) {
            return new UmpayHttpHandler(expectResponse,url);
        }
    },
    ICBC(){
        @Override
        public <T> T bankService(String service) {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public HttpHandler handler(ExpectResponseInterface expectResponse, Service service,String url) {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public HttpHandler handler(ExpectResponseInterface expectResponse,String url) {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }
    };
    public abstract <T>T bankService(String service);

    public abstract HttpHandler handler(ExpectResponseInterface expectResponse,Service service,String url);

    public abstract HttpHandler handler(ExpectResponseInterface expectResponse,String url);

    public  HttpHandler handler(File dir){return null;};
}
