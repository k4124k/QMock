package com.qmock.pay.anno;

import java.lang.annotation.*;

/**
 * 这个标签用于异步回调。
 * @author tianqing.wang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface CallBack {
    String value();
}
