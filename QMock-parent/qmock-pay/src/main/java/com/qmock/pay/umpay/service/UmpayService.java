package com.qmock.pay.umpay.service;

import com.qmock.pay.runner.Service;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import org.apache.log4j.Logger;


/**
 * @author tianqing.wang
 */
public enum UmpayService {

    AUTH_DIRECT_QUERY(){
        @Override
        public  Service service(UmpayHttpRequest resquest) {
            return new UmpayAuthDirectQueryService(resquest);
        }
    },
    DOWNLOAD_SETTLE_FILE(){
        @Override
        public Service service(UmpayHttpRequest resquest) {
            return new UmpayDownloadSettleFileService(resquest);
        }
    },
    MER_CANCEL(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayMerCancelService(request);
        }
    },
    MER_REFUND(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayMerRefundService(request);
        }
    },
    PAY_ANALYSIS(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPayAnalysisService(request);
        }
    },
    PAY_REQ(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPayReqService(request);
        }
    },
    PAY_RESULT_NOTIFY(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPayResultNotifyService(request);
        }
    },
    PRE_AUTH_DIRECT_CANCEL(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPreAuthDirectCancelService(request);
        }
    },
    PRE_AUTH_DIRECT_PAY(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPreAuthDirectPayService(request);
        }
    },
    PRE_AUTH_DIRECT_REFUND(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPreAuthDirectRefundService(request);
        }
    },
    PRE_AUTH_DIRECT_REQ(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPreAuthDirectReqService(request);
        }
    },
    PRE_AUTH_DIRECT_SETTLE(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPreAuthDirectSettleService(request);
        }
    },
    PRE_AUTH_NOTIFY(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayPreAuthNotifyService(request);
        }
    },
    QUERY_ORDER(){
        @Override
        public Service service(UmpayHttpRequest request) {
            return new UmpayQueryOrderService(request);
        }
    },
    CREDIT_ORDER_ANALYSIS(){
        public Service service(UmpayHttpRequest request){
            return new UmpayCreditOrderAnalysisService(request);
        }
    },
    CREDIT_DIRECT_PAY(){
        public Service service(UmpayHttpRequest request){
            logger.info("通过服务名映射到credit_direct_pay服务类进行业务处理");
            return new UmpayCreditDirectPayService(request);
        }
    },
    DEBIT_DIRECT_PAY(){
        public Service service(UmpayHttpRequest request){
            return new UmpayDebitDirectPayService(request);
        }
    },
    MER_ORDER_REFUND_QUERY(){
        public Service service(UmpayHttpRequest request){
            return new UmpayMerOrderRefundQueryService(request);
        }
    },
    MER_REFUNDNO_QUERY(){
        public Service service(UmpayHttpRequest request){
            return new UmpayMerRefundnoQueryService(request);
        }
    },
    SPLIT_REQ_RESULT(){
        public Service service(UmpayHttpRequest request){
            return new UmpaySplitReqResultService(request);
        }
    },
    PAY_REQ_SPLIT_BACK(){
        public Service service(UmpayHttpRequest request){
            return new UmpayPayReqSplitBackService(request);
        }
    },
    SPLIT_REQ(){
        public Service service(UmpayHttpRequest request){
            return new UmpaySplitReqService(request);
        }
    },
    SPLIT_REFUND_REQ(){
        public Service service(UmpayHttpRequest request){
            return new UmpaySplitRefundReqService(request);
        }
    },
    SPLIT_REFUND_RESULT(){
        public Service service(UmpayHttpRequest request){
            return new UmpaySplitRefundResultService(request);
        }
    };
    private static Logger logger = Logger.getLogger(UmpayService.class);
    public abstract Service service(UmpayHttpRequest request);
}
