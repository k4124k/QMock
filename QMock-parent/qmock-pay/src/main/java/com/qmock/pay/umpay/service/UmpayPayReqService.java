package com.qmock.pay.umpay.service;

import com.qmock.pay.common.DateFormat;
import com.qmock.pay.exception.SignEncException;
import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.runner.Service;
import com.qmock.pay.security.UmpayRSA;
import com.qmock.pay.umpay.UmpayParameters;
import com.qmock.pay.umpay.UmpayStringUtils;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import com.qmock.pay.umpay.handler.UmpayHttpResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tianqing.wang
 */
public class UmpayPayReqService implements Service {
    private UmpayHttpRequest umpayHttpRequest;
    private Map<String,String> defaultParams;
    public UmpayPayReqService(UmpayHttpRequest resquest){
        this.umpayHttpRequest=resquest;
        this.defaultParams=new HashMap<String, String>();
        defaultParams.put("sign_type","RSA");
        defaultParams.put("",this.umpayHttpRequest.getParameterValue("charset"));
        defaultParams.put("version","4.0");
        defaultParams.put("mer_id","商户编号");
        defaultParams.put("token","deal"+DateFormat.format("YYYYMMDDHHMMSS"));
        defaultParams.put("order_id",resquest.getParameterValue("ord_id"));
        defaultParams.put("mer_date",resquest.getParameterValue("mer_date"));
        defaultParams.put(UmpayParameters.TRADE_STATE,"详见交易状态说明");
        defaultParams.put(UmpayParameters.TRADE_NO,"嗖付交易账号");
        defaultParams.put(UmpayParameters.RET_CODE,"详见附录");
    }

    @Override
    public String getHttpResponseContent(ExpectResponse expectResponse) throws SignEncException {
        Map<String,String> responseparams = expectResponse.getResponse();
        this.defaultParams.putAll(responseparams);
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign",UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public String getHttpResponseContent() throws SignEncException {
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign",UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public Map<String, String> getResponseParameters() {
        return this.defaultParams;
    }
}
