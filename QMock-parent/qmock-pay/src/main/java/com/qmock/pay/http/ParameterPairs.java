package com.qmock.pay.http;

import java.util.*;

/**
 * 这个类只是一个参数对
 * @author tianqing.wang
 */
public class ParameterPairs {
    private Map<String,String> pairs;
    public ParameterPairs(){
        this.pairs=new HashMap<String, String>();
    }
    public ParameterPairs(Map<String,String> map){
        this.pairs=map;
    }

    public void add(String key,String value){
       this.pairs.put(key,value);
    }
    public void add(Map<String,String> maps){
        this.pairs.putAll(maps);
    }

    public String remove(String key){
        return this.pairs.remove(key);
    }
    public Map<String,String> getPairs(){
        return this.pairs;
    }
}
