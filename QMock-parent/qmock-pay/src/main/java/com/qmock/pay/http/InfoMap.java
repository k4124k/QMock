package com.qmock.pay.http;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class InfoMap{
    private static Map<String,ExpectResponseInterface> infomap=new HashMap<String,ExpectResponseInterface>();
    public static ExpectResponseInterface getExpectResponse(String ip){
        return infomap.get(ip);
    }

    public static void addInfo(String ip,ExpectResponseInterface responseInterface){
        infomap.put(ip,responseInterface);
    }

    public static void remove(String ip){
        infomap.remove(ip);
    }

    public static void clear(){
        infomap.clear();
    }

    public static boolean isContains(String ip){
        return infomap.containsKey(ip);
    }

    public static Map<String,ExpectResponseInterface> map(){
        return infomap;
    }
}
