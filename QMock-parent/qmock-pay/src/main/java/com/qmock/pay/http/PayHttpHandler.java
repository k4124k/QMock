package com.qmock.pay.http;

import com.qmock.pay.runner.Service;
import org.webbitserver.HttpHandler;

/**
 * @author tianqing.wang
 */
public abstract class PayHttpHandler implements HttpHandler {
    private ExpectResponse response;
    private Service service;
    public PayHttpHandler(ExpectResponse response,Service service){
        this.response=response;
        this.service=service;
    }
}
