package com.qmock.pay.umpay;

/**
 * @author tianqing.wang
 */
public class UmpayParameters {
    //--------------------协议参数-----------------
    /**接口服务名称*/
    public static final String SERVICE="service";
    /***参数字符编码集*/
    public static final String CHARSET="charset";
    /**商户编号*/
    public static final String MER_ID="mer_id";
    /**签名方式*/
    public static final String SIGN_TYPE="sign_type";
    /**签名*/
    public static final String SIGN = "sign";
    /**页面跳转同步通知页面路径*/
    public static final String RET_URL="ret_url";
    /**页面跳转异步通知页面路径*/
    public static final String NOTIFY_URL="notify_url";
    /**响应数据格式 //暂为html格式*/
    public static final String RES_FORMAT="res_format";
    /**版本号 定值4.0*/
    public static final String VERSION="version";

    //----------------------业务参数----------------------
    /**商品号*/
    public static final String GOODS_ID="goods_id";
    /**商品描述信息*/
    public static final String GOODS_INF="goods_inf";
    /**媒介标识*/
    public static final String MEDIA_ID="media_id";
    /**媒介类型   手机号*/
    public static final String MEDIA_TYPE="media_type";
    /**商户唯一订单号*/
    public static final String ORDER_ID="order_id";
    /**商户订单日期  YYYYMMDD**/
    public static final String MER_DATE="mer_date";
    /**付款金额   定长13位**/
    public static final String AMOUNT="amount";
    /**付款币种**/
    public static final String AMT_TYPE="amt_type";
    /**默认支付方式**/
    public static final String PAY_TYPE="pay_type";
    /**默认银行**/
    public static final String GATE_ID="gate_id";
    /**商户私有域**/
    public static final String MER_PRIV="mer_priv";
    /**用户IP地址*/
    public static final String USER_IP="user_ip";
    /**业务扩展信息*/
    public static final String EXPAND="expand";
    /**订单过期时长**/
    public static final String EXPIRE_TIME="expire_time";
    /**分账数据*/
    public static final String SPLIT_DATA="split_data";
    /**分账类型**
     */
    public static final String SPLIT_TYPE="split_type";
    /**支付请求令牌----- 异步参数*/
    public static final String TOKEN = "token";
    /**交易状态---- 异步参数*/
    public static final String TRADE_STATE="trade_state";
    /***嗖付交易号 -> 异步参数*/
    public static final String TRADE_NO="trade_no";
    /**返回码 -> 异步参数*/
    public static final String RET_CODE="ret_code";
    /**返回信息 ->异步参数*/
    public static final String RET_MSG="ret_msg";
    /**清算日期 -> 异步参数*/
    public static final String SETTLE_DATE="settle_date";
    /**银行流水账号 ->异步参数*/
    public static final String PAY_SEQ="pay_seq";
    /**交易错误码 ->异步参数*/
    public static final String ERROR_CODE="error_code";
    /**产品号*/
    public static final String PRODUCT_ID="product_id";
    /**已退金额*/
    public static final String REFUND_AMT="refund_amt";
    /**原始订单金额 ->退款参数*/
    public static final String ORG_AMOUNT="org_amount";
    /***退款金额*/
    public static final String REFUND_AMOUNT="refund_amount";
    //
    public static final String PAY_DATE="pay_date";
    /***申请金额，预授权查询*/
    public static final String REQ_AMT="req_amt";
    /**预授权*/
    public static final String PAY_AMT="pay_amt";
    /**退款流水号*/
    public static final String REFUND_NO="refund_no";



}
