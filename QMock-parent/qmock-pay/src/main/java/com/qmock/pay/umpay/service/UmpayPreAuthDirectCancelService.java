package com.qmock.pay.umpay.service;

import com.qmock.pay.common.DateFormat;
import com.qmock.pay.exception.SignEncException;
import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.runner.Service;
import com.qmock.pay.security.UmpayRSA;
import com.qmock.pay.umpay.UmpayParameters;
import com.qmock.pay.umpay.UmpayStringUtils;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import com.qmock.pay.umpay.handler.UmpayHttpResponse;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tianqing.wang
 */
public class UmpayPreAuthDirectCancelService implements Service {
    private UmpayHttpRequest resquest;
    private Map<String,String> defaultParams;
    public UmpayPreAuthDirectCancelService(UmpayHttpRequest resquest){
        this.resquest=resquest;
        this.defaultParams=new HashMap<String, String>();
        defaultParams.put("sign_type","RSA");
        defaultParams.put("charset",this.resquest.getParameterValue("charset"));
        defaultParams.put("version","4.0");
        defaultParams.put("mer_id",resquest.getParameterValue("mer_id"));
        defaultParams.put("order_id",resquest.getParameterValue("ord_id"));
        defaultParams.put("mer_date",resquest.getParameterValue("mer_date"));
        defaultParams.put(UmpayParameters.TRADE_STATE,"TRADE_FAIL");
        defaultParams.put(UmpayParameters.TRADE_NO,new Date().toString());
        defaultParams.put(UmpayParameters.RET_CODE,"0000");
    }

    @Override
    public String getHttpResponseContent(ExpectResponse expectResponse) throws SignEncException {
        Map<String,String> responseparams = expectResponse.getResponse();
        this.defaultParams.putAll(responseparams);
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign", UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public String getHttpResponseContent() throws SignEncException {
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign",UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public Map<String, String> getResponseParameters() {
        return this.defaultParams;
    }
}
