package com.qmock.pay.common;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期格式化的工具类
 * @author tianqing.wang
 */
public class DateFormat {

    public static String format(String format){
        return new SimpleDateFormat(format).format(new Date());
    }
}
