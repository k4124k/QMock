package com.qmock.pay.http;

import org.apache.http.HttpException;
import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



/**
 * @author tianqing.wang
 */
public class PayHttpRequest {
    private HttpClient httpClient;
    private HttpMessage httpUriRequest;
    public PayHttpRequest(){
        this.httpClient=new DefaultHttpClient();
        //请求超时
        httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,60000);
        //读取超时
        httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,60000);
    }
    public PayHttpRequest addHeader(String name,String value){
        this.httpUriRequest.addHeader(name,value);
        return this;
    }
    public PayHttpRequest get(String baseUrl){
        this.httpUriRequest=new HttpGet(baseUrl);
        return this;
    }

    public PayHttpRequest post(Map<String,String> parameters,String baseUrl){
        List<NameValuePair> parameter = new ArrayList<NameValuePair>();
        for(Map.Entry<String,String> param:parameters.entrySet()){
            parameter.add(new BasicNameValuePair(param.getKey(),param.getValue()));
        }
        this.httpUriRequest=new HttpPost(baseUrl);
        try {
            ((HttpPost)this.httpUriRequest).setEntity(new UrlEncodedFormEntity(parameter));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return this;
    }
    public PayHttpRequest post(String baseUrl){
        this.httpUriRequest=new HttpPost(baseUrl);
        return this;
    }

    public PayHttpResponse execute() throws HttpException {
        try {
            return new PayHttpResponse(this.httpClient.execute((HttpUriRequest)this.httpUriRequest));
        } catch (IOException e) {
            e.printStackTrace();
            throw new HttpException("http连接请求出现了错误，请检查网络！");
        }
    }
    public void close(){
        ((HttpUriRequest)this.httpUriRequest).abort();
        this.httpClient.getConnectionManager().shutdown();
    }

    public PayHttpRequest with(Map<String,String> params){
        List<NameValuePair> paramlist = new ArrayList<NameValuePair>();
        for(Map.Entry<String,String> entry:params.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            paramlist.add(new BasicNameValuePair(key,value));
        }
        try {
            ((HttpPost)this.httpUriRequest).setEntity(new UrlEncodedFormEntity(paramlist));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return this;
    }
}
