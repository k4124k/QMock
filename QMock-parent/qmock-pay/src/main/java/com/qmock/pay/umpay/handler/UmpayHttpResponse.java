package com.qmock.pay.umpay.handler;

/**
 * @author tianqing.wang
 */
public class UmpayHttpResponse {

    public static String format(String responseContent){
        return "<META NAME=\"MobilePayPlatform\" CONTENT=\""+responseContent+"\">";
    }
}
