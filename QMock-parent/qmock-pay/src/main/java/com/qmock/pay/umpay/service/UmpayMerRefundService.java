package com.qmock.pay.umpay.service;

import com.qmock.pay.common.DateFormat;
import com.qmock.pay.exception.SignEncException;
import com.qmock.pay.http.ExpectResponse;
import com.qmock.pay.runner.Service;
import com.qmock.pay.security.UmpayRSA;
import com.qmock.pay.umpay.UmpayStringUtils;
import com.qmock.pay.umpay.handler.UmpayHttpRequest;
import com.qmock.pay.umpay.handler.UmpayHttpResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * 这个类是信用卡直连商户退费服务类
 * @author tianqing.wang
 */
public class UmpayMerRefundService implements Service {
    private UmpayHttpRequest resquest;
    private Map<String,String> defaultParams;
    public UmpayMerRefundService(UmpayHttpRequest resquest){
        this.resquest=resquest;
        this.defaultParams=new HashMap<String, String>();
        this.defaultParams.put("sign_type","RSA");
        this.defaultParams.put("mer_id",resquest.getParameterValue("mer_id"));
        this.defaultParams.put("version","4.0");
        this.defaultParams.put("refund_no",resquest.getParameterValue("refund_no"));
        this.defaultParams.put("order_id",resquest.getParameterValue("order_id"));
        this.defaultParams.put("mer_date", DateFormat.format("YYYYMMDD"));
        this.defaultParams.put("amount",resquest.getParameterValue("amount"));
        this.defaultParams.put("trade_no",DateFormat.format("YYYYMMDDHHMMSS"));
        this.defaultParams.put("refund_state","详见附录");
        this.defaultParams.put("ret_code","详见附录");
    }

    @Override
    public String getHttpResponseContent(ExpectResponse expectResponse) throws SignEncException {
        Map<String,String> responseparams = expectResponse.getResponse();
        this.defaultParams.putAll(responseparams);
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign",UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public String getHttpResponseContent() throws SignEncException {
        String signString = UmpayStringUtils.toSignString(this.defaultParams);
        this.defaultParams.put("sign", UmpayRSA.getInstance().sign(signString));
        String responseContent = UmpayStringUtils.toString(defaultParams);
        return UmpayHttpResponse.format(responseContent);
    }

    @Override
    public Map<String, String> getResponseParameters() {
        return this.defaultParams;
    }
}
