package com.qmock.pay.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created with IntelliJ IDEA.
 * User: tianqing.wang
 * Date: 13-8-8
 * Time: 下午2:25
 * To change this template use File | Settings | File Templates.
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Url {
    String value();
}
