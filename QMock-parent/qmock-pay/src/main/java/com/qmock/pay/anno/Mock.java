package com.qmock.pay.anno;

import com.qmock.pay.runner.Bank;

import java.lang.annotation.*;

/**
 * @author tianqing.wang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Inherited
public @interface Mock {
    Bank value();
    String service() default "";
}
