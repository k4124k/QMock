package com.qmock.pay.http;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tianqing.wang
 */
public class PayHttpResponse {
    private HttpResponse httpResponse;
    public PayHttpResponse(HttpResponse response){
        this.httpResponse=response;
    }

    public int code(){
        return this.httpResponse.getStatusLine().getStatusCode();
    }

    public Map<String,String> getHeaders(){
        Map<String,String> headerMap = new HashMap<String,String>();
        Header[] headers = this.httpResponse.getAllHeaders();
        for(Header header:headers){
            headerMap.put(header.getName(),header.getValue());
        }
        return headerMap;
    }

    public String getHeaderValue(String name){
        return getHeaders().get(name);
    }

    public HttpEntity getHttpEntity(){
        return this.httpResponse.getEntity();
    }

    public String body(){
        try {
           // System.out.println(getHttpEntity().getContentEncoding().getValue());
            return EntityUtils.toString(getHttpEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
