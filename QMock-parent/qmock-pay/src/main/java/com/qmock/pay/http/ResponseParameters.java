package com.qmock.pay.http;

/**
 * @author tianqing.wang
 */
public interface ResponseParameters {

    public ParameterPairs toPairs();
}
