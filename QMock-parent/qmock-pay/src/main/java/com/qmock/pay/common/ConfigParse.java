package com.qmock.pay.common;

import org.qmock.runner.PropertiesTools;
import org.qmock.runner.exception.ConfigWrongException;

import java.io.File;
import java.util.Map;

/**
 * @author tianqing.wang
 */
public class ConfigParse {
    private static Map<String,String> parse(){
        if(!new File("resource"+File.separator+"config.properties").exists()){
            throw new ConfigWrongException("config配置信息无法扫描到，也许是无法找到config.properties文件！请检查配置文件");
        }
        return PropertiesTools.getPropertiesMap("resource" + File.separator + "config.properties");
    }

    public static String getPrivateKeyPath(){
        return parse().get("private_key");
    }

    public static String getPublicKeyPath(){
        return parse().get("public_key");
    }

    public static String getKeyPassword(){
        return parse().get("key_password");
    }

    public static String getRequestXMLFile(){
        return parse().get("request_xml_file");
    }

    public static String getResponseXMLFile(){
        return parse().get("response_xml_file");
    }

    /**返回我们设置的Base-name的值*/
    public static String getBaseName(){
        return parse().get("base-name");
    }

    public static String serverUrl(){
        return parse().get("server_url").trim();
    }
}
