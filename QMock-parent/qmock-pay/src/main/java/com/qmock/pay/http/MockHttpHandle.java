package com.qmock.pay.http;

import com.qmock.pay.runner.Service;
import org.apache.log4j.Logger;
import org.webbitserver.HttpControl;
import org.webbitserver.HttpHandler;
import org.webbitserver.HttpRequest;
import org.webbitserver.HttpResponse;
import org.webbitserver.handler.StaticFileHandler;
import org.webbitserver.handler.TemplateEngine;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;

/**
 * @author tianqing.wang
 */
public class MockHttpHandle extends StaticFileHandler {
    public MockHttpHandle(File dir, Executor ioThread, TemplateEngine templateEngine) {
        super(dir, ioThread, templateEngine);
    }

    public MockHttpHandle(File dir){
        super(dir);
    }
}
