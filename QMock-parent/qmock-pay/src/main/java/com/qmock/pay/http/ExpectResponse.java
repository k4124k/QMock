package com.qmock.pay.http;

import java.util.Map;


/**
 * 这个类用来处理我们期望的响应数据。通过注解来实现我们的响应需要返回哪些数据。否则，数据都实行默认。
 * @author tianqing.wang
 */
public class ExpectResponse implements ExpectResponseInterface{
    private ParameterPairs parameterPairs;
    public ExpectResponse(ParameterPairs parameterPairs){
        this.parameterPairs=parameterPairs;
    }
    @Override
    public Map<String,String> getResponse() {
        return this.parameterPairs.getPairs();
    }

    public String toString(){
        StringBuilder stringBuilder=new StringBuilder();
        for(Map.Entry<String,String> entry:getResponse().entrySet()){
            stringBuilder.append("key->"+entry.getKey()+", value->"+entry.getValue());
        }
        return stringBuilder.toString();
    }
}
