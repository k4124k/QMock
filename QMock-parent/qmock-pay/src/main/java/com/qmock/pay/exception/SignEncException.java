package com.qmock.pay.exception;

/**
 * Created with IntelliJ IDEA.
 * User: tianqing.wang
 * Date: 13-8-9
 * Time: 上午11:00
 * To change this template use File | Settings | File Templates.
 */
public class SignEncException extends Exception {

    public SignEncException(){
        super();
    }

    public SignEncException(String message){
        super(message);
    }

    public SignEncException(String message,Exception exception){
        super(message,exception);
    }
}
