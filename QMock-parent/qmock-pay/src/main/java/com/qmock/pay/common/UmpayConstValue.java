package com.qmock.pay.common;

/**
 * @author tianqing.wang
 */
public class UmpayConstValue {
    /**密钥路径pxf*/
    public final static String PRIVATE_KEY_FILE=ConfigParse.getPrivateKeyPath();
    /**公钥路径*/
    public final static String PUBLIC_KEY_FILE=ConfigParse.getPublicKeyPath();
    /**联动优势版本号*/
    public final static String UMPAY_VERSION="4.0";
    /**暂时保留的两个参数，xml的文件地址，现在并没有使用，防止后期使用。*/
    public final static String REQUEST_XML_FILE=ConfigParse.getRequestXMLFile();

    public final static String RESPONSE_XML_FILE=ConfigParse.getResponseXMLFile();

    public final static String PRIVATE_PASSWORD=ConfigParse.getKeyPassword();
}
